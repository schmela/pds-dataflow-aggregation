

void print_flow(struct flow *fl);

#ifndef FLOW_H
#define FLOW_H

#include <sys/types.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <cstdint>
#include <cstring>



// dvojice pocet paketu a pocet bytu
typedef std::pair<long long, long long> valuePair;

// NA AREGACI PODLE IP ADRES
typedef struct in6_addr aggreg_key_ipv6;
typedef u_long aggreg_key_ipv4;

// operator < aby mohl byt klic std::map struct in6_addr
bool operator <( aggreg_key_ipv6 const& left, aggreg_key_ipv6 const& right )
{
    for (int i=0; i< 16; i++)
    {
    	if (left.s6_addr[i] < right.s6_addr[i]) return 1;
    	if (left.s6_addr[i] > right.s6_addr[i]) return 0;
    }
    return 0;
}


// NA AGREGACI PODLE PORTU
typedef uint16_t aggreg_key_port;


// NA SORTOVANI PORTU
typedef std::pair <aggreg_key_port,valuePair> sort_vector_port;

// funkce pro porovnani 2 instanci sort_vector_port
bool vectorCmp_bytes(sort_vector_port a, sort_vector_port b)
{
	return ( a.second.second >= b.second.second);
}

bool vectorCmp_packets(sort_vector_port a, sort_vector_port b)
{
	return ( a.second.first >= b.second.first);
}

// struktura flow, do ni se nacita ze souboru
struct flow {
	uint32_t		sa_family;
	struct in6_addr src_addr;
	struct in6_addr dst_addr;
	uint16_t		src_port;
	uint16_t		dst_port;
	uint64_t        packets;
	uint64_t        bytes;
}; 

#endif