#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <netinet/ip.h>			// site
#include <netinet/ip6.h>		// site
#include <arpa/inet.h>			// site
#include <dirent.h>				// na adresare
#include <limits.h>
//#include <omp.h>


#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <cstdint>
#include <sstream>
//#include <parallel/algorithm>			// std::sort
#include <algorithm>			// std::sort
#include <unordered_map>

#include "flow.h"


using namespace std;

void print_usage_and_exit()
{
	cout 
	<< "flow -f directory -a aggregation -s sort" << endl
	<< "========================================" << endl
	<< "directory - adresar s daty" << endl
	<< "aggregation - podle ceho se agreguje [src,dst][ip/%d,port]" << endl
	<< "sort - podle ceho se data setridi [bytes/packets]" << endl;
}

void list_files_in_directory(string dir_name, vector<string> *directory_vector)
{

	if (dir_name.back() == '/')
		//dir_name.pop_back();
		dir_name.erase(dir_name.size() - 1);

	DIR * d = NULL;
	d = opendir (dir_name.c_str());	

	if (d == NULL)
	{
		cerr << "zadany adresar neexistuje" << endl;
		exit (EXIT_FAILURE);
	}	

    while (1) 
    {
        struct dirent * entry;
        const char * d_name;

        // readdir -> nacte dalsi soubor z adresare
        entry = readdir (d);
        if (! entry) {
			// zadne dalsi soubory v adresari -> ukonceni rekurze;
            break;
        }
        d_name = entry->d_name;

        string dalsi = dir_name + "/" + d_name;

        DIR * dir_or_file = NULL;
        dir_or_file = opendir (dalsi.c_str());
        bool is_dir = (dir_or_file != NULL);

        if (is_dir) 
        {

            // vynechame adresare . (tento adresar) a .. (nadrazeny adresar)
            if (strcmp (d_name, "..") != 0 &&
                strcmp (d_name, ".") != 0) 
            {
                int path_length;
                char path[PATH_MAX];
 
                path_length = snprintf (path, PATH_MAX,
                                        "%s/%s", dir_name.c_str(), d_name);
                //printf ("%s\n", path);
                if (path_length >= PATH_MAX) {
                    cerr << "prilis dlouha cesta" << endl;
                    exit (EXIT_FAILURE);
                }
                // rekurzivni volani na zanoreny adresar
                list_files_in_directory (path,directory_vector);
            }
        }
        else
        {
        	directory_vector->push_back(dir_name+"/"+d_name);
        	//cout << dir_name << "/" << d_name << endl;
        }
    }

    // zavreni adresare
    if (closedir (d)) {
        cerr << "nelze zavrit adresar " <<  dir_name << endl;
        exit (EXIT_FAILURE);
    }

}

int main(int argc, char *argv[])
{

	/*****************************************************/
	// NACTENI A KONTROLA PARAMETRU
	/*****************************************************/

	int opt;
	if (argc < 3) {
		print_usage_and_exit();
		return (EXIT_FAILURE);
	}

	string aggreg="", sort="", filename="";	

	while ((opt = getopt(argc, argv, "f:a:s:")) != -1) {
		switch (opt) {
			case 'f':
				filename = optarg;
				break;
			case 'a':
				aggreg = optarg;
				break;
			case 's':
				sort = optarg;
				break;								
			default:
				print_usage_and_exit();
				return (EXIT_FAILURE);
		}	
	}

	if ( (filename=="") || (sort=="") || (aggreg==""))
	{
		print_usage_and_exit();
		return (EXIT_FAILURE);
	}

	// SEZNAM VSECH SOUBORU V ADRESARI
	////////////////////////////////////////

	std::vector<string> *soubory;
	soubory = new std::vector<string>;
	list_files_in_directory (filename,soubory);

	/* KONTROLA A ZPRACOVANI PARAMETRU -s  */	
	//////////////////////////////////////////

	if ((aggreg.length() > 10) || (sort != "bytes" && sort != "packets" ))
	{
		print_usage_and_exit();
		return (EXIT_FAILURE);
	}

	bool sort_bytes;
	if (sort=="bytes")
		sort_bytes = true;
	else
		sort_bytes = false;


	/* KONTROLA A ZPRACOVANI PARAMETRU -a  */	
	//////////////////////////////////////////

	int only_ip_version = 0;
	int delka_masky_v4 = 32;
	int delka_masky_v6 = 128;
	int source = 0;
	int port = 0;
	bool maskovat = true;

	if ( aggreg == "srcip" ) { source=true; port=false; maskovat=false;}
	else if ( aggreg == "srcip4" ) { source=true; port=false; only_ip_version=4; }
	else if ( aggreg == "srcip6" ) { source=true; port=false; maskovat = false; only_ip_version=6; }
	else if ( sscanf(aggreg.c_str(), "srcip/%d", &delka_masky_v4) == 1) { source=true; port=false; delka_masky_v6=delka_masky_v4;}
	else if ( sscanf(aggreg.c_str(), "srcip4/%d", &delka_masky_v4) == 1) { source=true; port=false; only_ip_version=4;}
	else if ( sscanf(aggreg.c_str(), "srcip6/%d", &delka_masky_v6) == 1) { source=true; port=false; only_ip_version=6;}

	else if ( aggreg == "dstip" ) { source=false; port=false; }
	else if ( aggreg == "dstip4" ) { source=false; port=false;  only_ip_version=4;}
	else if ( aggreg == "dstip6" ) { source=false; port=false; maskovat = false; only_ip_version=6;}
	else if ( sscanf(aggreg.c_str(), "dstip/%d",  &delka_masky_v4) == 1) { source=false; port=false; delka_masky_v6=delka_masky_v4;}
	else if ( sscanf(aggreg.c_str(), "dstip4/%d", &delka_masky_v4) == 1) { source=false; port=false; only_ip_version=4;}
	else if ( sscanf(aggreg.c_str(), "dstip6/%d", &delka_masky_v6) == 1) { source=false; port=false; only_ip_version=6;}

	else if ( aggreg == "srcport" ) { source=true;  port=true;}
	else if ( aggreg == "dstport" ) { source=false; port=true;}
	else
	{
		cerr << "spatne zadana aggregace" << endl;
		print_usage_and_exit();
		return (EXIT_FAILURE);
	}

	if ( (delka_masky_v4 > 32)  || (delka_masky_v4 < 0) ||
		 (delka_masky_v6 > 128) || (delka_masky_v6 < 0) )
	{
		cerr << "spatne zadana aggregace - neplatna delka masky" << endl;
		print_usage_and_exit();
		return (EXIT_FAILURE);
	}		


	/*****************************************************/
	// AGREGACE PODLE IP
	/*****************************************************/	
	if (! port)
	{
		// mapa pro IPv4
		std::unordered_map<aggreg_key_ipv4, valuePair> * aggreg_map_ipv4;
		aggreg_map_ipv4 = new std::unordered_map<aggreg_key_ipv4, valuePair>;		

		// mapa pro IPv6
		std::map<aggreg_key_ipv6, valuePair> * aggreg_map_ipv6;
		aggreg_map_ipv6 = new std::map<aggreg_key_ipv6, valuePair>;		


		// PRIPRAVA MASKY IPv6

		// vynulovani celych nepotrebnych oktetu
		int zachovat_octetu = ( ((delka_masky_v6-1)/8) + 1 );	

		int zachovat_bitu = delka_masky_v4 % 8;
		//cout << "zachovat bitu "<< zachovat_bitu << endl;
		uint8_t bit_maska = ~0;
		//cout << "bit. maska "<< int(bit_maska) << endl;
		bit_maska <<= (8-zachovat_bitu);
		//cout << "bit. maska "<< int(bit_maska) << endl;


		// PRIPRAVA MASKY IPv4

		u_long bit_maska_ipv4 = ~0;
		bit_maska_ipv4 <<= (delka_masky_v4);
		bit_maska_ipv4 = ~bit_maska_ipv4;

		// na ulozeni adresy se kterou se bude pracovat - aby se furt nemuselo resit src/dst
		aggreg_key_ipv6 ipv6_address;
		aggreg_key_ipv4 ipv4_address;

		typedef std::vector <string>::iterator it_string_vec;
		for(it_string_vec iterator = soubory->begin(); iterator != soubory->end(); iterator++) 
		{

			// otevreni souboru
			FILE *fp = fopen(iterator->c_str(), "rb");
			struct flow fl;
			size_t n = 0;

			while ((n = fread(&fl, sizeof(struct flow), 1, fp)) != 0) 
			{
						
				// SRC / DST - dal pracujeme jen s promennou ipvX_address
				if (source) 
					ipv6_address = fl.src_addr;
				else
					//inet_ntop(AF_INET6, &(fl.dst_addr), ip_address, INET6_ADDRSTRLEN);
					ipv6_address = fl.dst_addr;


				///////////////////////////
				//    IPV4
				////////////////////////////
				if ( ntohl(fl.sa_family) == AF_INET) // IPV4
				{
					// POUZE IPV6 TOKY -> PRESKOCI SE
					//////////////////////////////////

					if (only_ip_version == 6)
					{
						//cerr << "prijimaji se pouze IPV6 zaznamy, preskoceno" << endl;
						continue;
					}

					// VYKOPIROVANI Z IPV6 STRUKTURY
					/////////////////////////////////

					memcpy( &ipv4_address, &ipv6_address.s6_addr[12], sizeof(aggreg_key_ipv4));

					// MASKOVANI
					/////////////////////////
					ipv4_address &= bit_maska_ipv4; 

					// PRIDANI INFORMACI DO MAPY
					/////////////////////////////

					// kdyz uz tam prvek existuje
					if (aggreg_map_ipv4->count(ipv4_address) == 1)
					{
						aggreg_map_ipv4->at(ipv4_address).first += (long long)__builtin_bswap64(fl.packets);
						aggreg_map_ipv4->at(ipv4_address).second += (long long)__builtin_bswap64(fl.bytes);
					}
					else
					{
						aggreg_map_ipv4->insert({ipv4_address,{__builtin_bswap64(fl.packets),__builtin_bswap64(fl.bytes)}});
					}
				}
				/////////////////
				// IPV6
				/////////////////
				else if ( ntohl(fl.sa_family) == AF_INET6) // IPV6
				{
					// POUZE IPV4 TOKY -> PRESKOCI SE
					//////////////////////////////////

					if (only_ip_version == 4)
					{
						//cerr << "prijimaji se pouze IPV4 zaznamy, preskoceno" << endl;
						continue;
					}

					// MASKOVANI
					////////////////////////////
					if (maskovat)
					{
						if (delka_masky_v6 == 0)
						{
							// kdyz je maska 0 -> vsechny oktety vynulovat
							for (uint8_t i=0; i<16; i++)
								ipv6_address.s6_addr[i] = 0; 
						}
						else 
						{			
							// vymaskovani oktetu, kde maji byt same nuly
							for (uint8_t i=zachovat_octetu; i<16; i++)
							{
								ipv6_address.s6_addr[i] = 0;
							}

							// castecne vymaskovani oktetu, kde je hranice nul a jednicek
							// pokud se ma zachovat 0 bitu -> nedela se nic...
							if (zachovat_bitu)
								ipv6_address.s6_addr[1+zachovat_octetu] &= bit_maska;
						}
					}

					// PRIDANI INFORMACI DO MAPY
					////////////////////////////////

					// kdyz uz tam prvek existuje
					if (aggreg_map_ipv6->count(ipv6_address) == 1)
					{
						aggreg_map_ipv6->at(ipv6_address).first += (long long)__builtin_bswap64(fl.packets);
						aggreg_map_ipv6->at(ipv6_address).second += (long long)__builtin_bswap64(fl.bytes);	
					}
					else
					{
						aggreg_map_ipv6->insert({ipv6_address,{__builtin_bswap64(fl.packets),__builtin_bswap64(fl.bytes)}});
					}
				}
				// kdyz je jina rodina nez IPV4 nebo IPV6
				else continue;			

			}

			fclose(fp);

		}

		// SORTOVANI
		////////////////////////////////

		std::multimap<long long, string> * sorted_map_ips;		
		sorted_map_ips = new std::multimap<long long, string>;

		// PRIDANI VSECH IPV4 FLOW
		typedef std::unordered_map<aggreg_key_ipv4, valuePair>::iterator map_it_v4;
		for(map_it_v4 iterator = aggreg_map_ipv4->begin(); iterator != aggreg_map_ipv4->end(); iterator++) 
		{

			if (aggreg_map_ipv4->count(iterator->first) == 1)
			{
				char str[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, &(iterator->first), str, INET_ADDRSTRLEN);

				std::stringstream ss;
				ss << str << "," << iterator->second.first <<  "," << iterator->second.second;
				string output_string = ss.str();

				if (sort_bytes)
					sorted_map_ips->insert({iterator->second.second,output_string});
				else
					sorted_map_ips->insert({iterator->second.first,output_string});
			}

		}

		// PRIDANI VSECH IPV6 FLOW
		typedef std::map<aggreg_key_ipv6, valuePair>::iterator map_it_v6;
		for(map_it_v6 iterator = aggreg_map_ipv6->begin(); iterator != aggreg_map_ipv6->end(); iterator++) 
		{
			if (aggreg_map_ipv6->count(iterator->first) == 1)
			{				
				char ip_buff[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, &(iterator->first), ip_buff, INET6_ADDRSTRLEN);

				//std::cout << ip_buff << "," << iterator->second.first <<  "," << iterator->second.second << std::endl;

				std::stringstream ss;
				ss << ip_buff << "," << iterator->second.first <<  "," << iterator->second.second;
				string output_string = ss.str();

				if (sort_bytes)
					sorted_map_ips->insert({iterator->second.second,output_string});
				else
					sorted_map_ips->insert({iterator->second.first,output_string});
			}

		}	

		// VYPIS
		///////////////////////////////////

		if (source) 
			std::cout << "#srcip,packets,bytes" << std::endl;
		else 
			std::cout << "#dstip,packets,bytes" << std::endl;

		typedef std::multimap<long long, string>::reverse_iterator map_sorted_it;
		for(map_sorted_it iterator = sorted_map_ips->rbegin(); iterator != sorted_map_ips->rend(); iterator++) 
		{
			cout << iterator->second << endl;
		}

		delete sorted_map_ips;
		delete aggreg_map_ipv6;
		delete aggreg_map_ipv4;

	}
	else
	{
		/*****************************************************/
		// AGREGACE PODLE PORTU
		/*****************************************************/	

		// hashovaci tabulka pro porty
		std::unordered_map<aggreg_key_port, valuePair > * aggreg_map_ports;
		aggreg_map_ports = new std::unordered_map<aggreg_key_port, valuePair >;

		uint16_t port = 0;

		// PRES VSECHNY SOUBORY
		//////////////////////////	

		typedef std::vector <string>::iterator it_string_vec;
		for(it_string_vec iterator = soubory->begin(); iterator != soubory->end(); iterator++)
		//#pragma omp parallel for
		//for (unsigned int i=0; i<soubory->size(); i++) 
		{
			FILE *fp = fopen(iterator->c_str(), "rb");
			//FILE *fp = fopen(soubory->at(i).c_str(), "rb");
			struct flow fl;
			size_t n = 0;

			// PRES VSECHNY ZAZNAMY V SOUBORU
			while ((n = fread(&fl, sizeof(struct flow), 1, fp)) != 0) 
			{
				if (source) 
					port = fl.src_port;
				else
					port = fl.dst_port;

				// kdyz uz tam prvek existuje
				if (aggreg_map_ports->count(port) == 1)
				{
					aggreg_map_ports->at(port).first += (long long)__builtin_bswap64(fl.packets);
					aggreg_map_ports->at(port).second += (long long)__builtin_bswap64(fl.bytes);
				}
				else
				{			
					aggreg_map_ports->insert({port,{__builtin_bswap64(fl.packets),__builtin_bswap64(fl.bytes)}});
				}
			}
			fclose(fp);	
			
		}
				
		// SORTOVANI
		////////////////////////
		std::vector <sort_vector_port> *sort_vector;
		sort_vector = new std::vector <sort_vector_port>(aggreg_map_ports->begin(), aggreg_map_ports->end());

		if (sort_bytes)
			std::sort(sort_vector->begin(),sort_vector->end(),&vectorCmp_bytes);
		else
			std::sort(sort_vector->begin(),sort_vector->end(),&vectorCmp_packets);

		// VYPIS
		////////////////////////
		if (source)
			std::cout << "#srcport,packets,bytes" << std::endl;
		else
			std::cout << "#dstport,packets,bytes" << std::endl;


		typedef std::vector <sort_vector_port>::iterator it_type;
		for(it_type iterator = sort_vector->begin(); iterator != sort_vector->end(); iterator++) 
		{
			std::cout << ntohs(iterator->first) << "," << iterator->second.first <<  "," << iterator->second.second << std::endl;
		}

		delete sort_vector;
		delete aggreg_map_ports;
				
	}

	delete soubory;

	
	return (EXIT_SUCCESS);
}
