CPP          = g++
CPPFLAGS     = -std=c++0x -pedantic -Wextra -g -O3 -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS
#CPPFLAGS     = -std=c++11 -fopenmp -pedantic -Wextra -g -O3 -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS

all: flow

flow: flow.o
	$(CPP) $(CPPFLAGS) -o flow flow.o

flow.o: flow.cpp flow.h
	$(CPP) $(CPPFLAGS) -c flow.cpp

clean:
	rm *.o flow

pack: clean
	tar czf xhrade08.tar.gz Makefile Readme *.cpp *.h *.pdf
