#!/bin/bash

slozka='../vstupy/00/'

echo $'\nsrcport \n----------'
time ./flow -f $slozka -a srcport -s bytes > a_srcport_s_bytes

echo $'\nsrcip \n----------'
time ./flow -f $slozka -a srcip -s bytes > a_srcip_s_bytes
echo $'\nsrcip/16 \n----------'
time ./flow -f $slozka -a srcip/16 -s bytes > a_srcip_16_s_bytes

echo $'\nsrcip4/0 \n----------'
time ./flow -f $slozka -a srcip4/0 -s bytes > a_srcip4_0_s_bytes
echo $'\nsrcip4/24 \n----------'
time ./flow -f $slozka -a srcip4/24 -s bytes > a_srcip4_24_s_bytes
echo $'\nsrcip4/28 \n----------'
time ./flow -f $slozka -a srcip4/28 -s bytes > a_srcip4_28_s_bytes
echo $'\nsrcip4/32 \n----------'
time ./flow -f $slozka -a srcip4/32 -s bytes > a_srcip4_32_s_bytes

echo $'\nsrcip6/0 \n----------'
time ./flow -f $slozka -a srcip6/0 -s bytes > a_srcip6_0_s_bytes
echo $'\nsrcip6/48 \n----------'
time ./flow -f $slozka -a srcip6/48 -s bytes > a_srcip6_48_s_bytes
echo $'\nsrcip6/68 \n----------'
time ./flow -f $slozka -a srcip6/68 -s bytes > a_srcip6_68_s_bytes
echo $'\nsrcip6/128 \n----------'
time ./flow -f $slozka -a srcip6/128 -s bytes > a_srcip6_128_s_bytes

